\documentclass[titlepage]{article}
\usepackage{titling}
\usepackage[margin=1in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{caption}
\usepackage[super]{cite}
\usepackage{subcaption}
\newcommand{\subtitle}[1]{%
  \posttitle{%
    \par\end{center}
    \begin{center}\Large#1\end{center}
    \vskip0.5em}%
}

\begin{document}

\title{Adaptive Multimesh Refinement Techniques for Solving Non-Linear Coupled Equations Applicable to Reactor Accident Transients}
\subtitle{Master's Thesis Proposal}
\author{Kevin Dugan}
\date{February 1, 2013}

\maketitle

\section{Introduction}
In reactor analysis, many of the physical processes occurring are strongly coupled. For example, fuel temperature affects the rate of fission in the fuel and the fission rate affects the temperature field --- mathematically this manifests through the temperature dependence of the macroscopic cross-sections. When modeling reactors, these interactions present themselves as non-linear coupling, which can be challenging to solve numerically. The proposed Master's Thesis is a study of how to efficiently and accurately solve non-linear, coupled equations simultaneously using Multimesh Adaptive Mesh Refinement (Multimesh AMR) technology.

The emerging technology of adaptive mesh refinement has been used in the past to study coupled physics problems. It has been used to achieve more accurate results for the criticality state of a reactor model in two and three dimensions\cite{hp-Yaqi, h-Yaqi} and has been used to study the coupling between the neutron flux and temperature\cite{hp-Coupled}. These studies show that using multimesh AMR leads to accurate solutions while saving on computing resources. One very important note to make is that, in these studies, multi-mesh refers to each physic having its own spatial discretization. Each physic may have differing spatial discretization needs and thus a single spatial mesh optimal for all physics is unreasonable; this adds difficulties when assembling the system of equations to solve, which is discussed in the \emph{Spatial Discretization} section. An interesting step will be to extend the technology by solving a realistic benchmark problem \cite{ANL} that mimics many of the challenging mathematical features found in real-life problems. 

\section{Formulation}
\subsection{Problem Model Description}
In general, reactor physics models need to include many physical phenomena --- Multigroup Diffusion, Delayed Neutron Precursors, and Heat Transfer. Equations (\ref{eq:Gen Fast Diffusion} - \ref{eq:Gen Temp}) represent a general system of reactor physics that will be referred to during this section.

\begin{equation}
    \label{eq:Gen Fast Diffusion}
    \frac{1}{v_1} \frac{\partial \phi_1}{\partial t} - \nabla D_1(T) \nabla \phi_1 + \Sigma_{r,1}(T) \phi_1 = \nu \beta [\Sigma_{f,1}(T) \phi_1 + \Sigma_{f,2}(T) \phi_2] + \sum_{i=1}^I \lambda_i C_i + Q_{\phi 1}
\end{equation}

\begin{equation}
	\label{eq:Gen Thermal Diffusion}
	\frac{1}{v_2} \frac{\partial \phi_2}{\partial t} - \nabla D_2(T) \nabla \phi_2 + \Sigma_{a,2}(T) \phi_2 = \Sigma_{s, 1 \to 2}(T) \phi_1 + Q_{\phi 2}
\end{equation}

\begin{equation}
	\label{eq:Gen Precursori}
	\frac{\partial C_i}{\partial t} = \nu \beta_i [\Sigma_{f,1}(T) \phi_1 + \Sigma_{f,2}(T) \phi_2 ] - \lambda_i C_i + Q_{C i}, \qquad 1 \leq i \leq I
\end{equation}

\begin{equation}
	\label{eq:Gen Temp}
	\frac{\partial T}{\partial t} - \nabla k(T) \nabla T = \alpha [\Sigma_{f,1}(T) \phi_1 + \Sigma_{f,2}(T) \phi_2 ] + Q_T
\end{equation}

\noindent
For many applications, two precursor groups is sufficient ($I =2$). This system of equations contains many challenging aspects --- Tight couplings, non-linear coefficients, etc. A number of mathematical techniques will be needed to solve such a system, which will be introduced in the remainder of this section.

\subsection{Newton's Method}
After spatial and temporal discretization, one obtains a non-linear residual function of the new time step unknowns $F(\vec{U}^{n+1})$. Newton's method will be used when solving non-linear equations, which involves finding the roots of the residual function [$F(\vec{U}) = 0$]. The sequence of a single Newton's iteration step\cite{KelleyNewton} is defined by

\begin{equation*}
J(\vec{U}^l) \delta \vec{U} = - F(\vec{U}^l)
\end{equation*}
\begin{equation*}
\vec{U}^{l+1} = \vec{U}^l + \delta \vec{U}
\end{equation*} 

\noindent
where the Jacobian matrix $J(\vec{U}^l)$ is formed analytically by differentiating each residual with respect to every unknown. 

\begin{equation*}
J(\vec{U}^l) = 
\begin{bmatrix}
\frac{\partial F^{\phi1}}{\partial \phi1} & \frac{\partial F^{\phi1}}{\partial \phi2} & \cdots & \frac{\partial F^{\phi1}}{\partial T}\\
\frac{\partial F^{\phi2}}{\partial \phi1} & \frac{\partial F^{\phi2}}{\partial \phi2} & \cdots & \frac{\partial F^{\phi2}}{\partial T}\\
\vdots & \vdots & \ddots & \vdots \\
\frac{\partial F^{T}}{\partial \phi1} & \frac{\partial F^{T}}{\partial \phi2} & \cdots & \frac{\partial F^{T}}{\partial T}\\
 \end{bmatrix}
=
\begin{bmatrix}
J^{\phi1 \phi1} & J^{\phi1 \phi2} & \cdots & J^{\phi1 T} \\
J^{\phi2 \phi1} & J^{\phi2 \phi2} & \cdots & J^{\phi2 T} \\
\vdots & \vdots & \ddots & \vdots \\
J^{T \phi1} & J^{T \phi2} & \cdots & J^{T T} \\
 \end{bmatrix}
\end{equation*}

\noindent
The following formulation of numerical methods is made to be compatible with Newton's method, thus all equations are presented in the form of residuals. Newton's method is used to solve the entire system of equations even if an equation is linear; Newton's method converges after one iteration for linear problems.

\subsection{Spatial Discretization}
The Continuous Galerkin Finite Element Method has been used to numerically solve many types of partial differential equations\cite{Segerlind1984}. The method involves spatially discretizing each solution domain and representing the solution as a linear combination of basis functions. By following the finite element method, a steady state residual can be assembled and used to numerically solve for the solution. Equation (\ref{eq:SS-res}) is an example of the system of equations (\ref{eq:Gen Fast Diffusion} - \ref{eq:Gen Temp}) after spatial discretization.

\begin{equation}
\label{eq:SS-res}
F^{SS} = \left[ \begin{array}{c} F_{\phi1}^{SS} \\ F_{\phi2}^{SS} \\ F_{C1}^{SS} \\ F_{C2}^{SS} \\ F_{T}^{SS}\end{array} \right] = 
\begin{bmatrix}
 K^{\phi_1 \phi_1}+M^{\phi_1 \phi_1} & M^{\phi_1 \phi_2} & M^{\phi_1 C_1} & M^{\phi_1 C_2} & 0\\
 M^{\phi_2 \phi_1} & K^{\phi_2 \phi_2} + M^{\phi_2 \phi_2} & 0 & 0 & \\
M^{C_1 \phi_1} & M^{C_1 \phi_2} & M^{C_1 C_1} & 0 & 0 \\
M^{C_2 \phi_1} & M^{C_2 \phi_2} & 0 & M^{C_2 C_2} & 0 \\
M^{T \phi_1} & M^{T \phi_2} & 0 & 0 & K^{T T}
 \end{bmatrix}
 \left[ \begin{array}{c} \phi_1 \\ \phi_2 \\ C_1 \\ C_2 \\ T \\ \end{array} \right]
 - \left[ \begin{array}{c} Q_{\phi1} \\ Q_{\phi2} \\ Q_{C1} \\ Q_{C2} \\ Q_{T} \end{array} \right]
\end{equation}

\noindent
Each $M^{a b}$ and $K^{a b}$ are the \emph{mass} and \emph{stiffness} matrices assembled from the spatial discretization over a physic's mesh; Each $a,b$ superscript corresponds to a physic component $[\phi_1, \phi_2, C_1, C_2, T]$.

The multi-mesh aspect of the formulation introduces complexities when assembling coupling terms between physics since the basis functions describing each physic will be defined over different domains. For example, the term $ \left( b_i^{\phi2}, -\Sigma_{s, 1\to 2} b_j^{\phi1} \right)$ involves integrating the product of two basis functions over a cell in the triangulation of $\phi_2$, but the cells for each set of basis functions may not be the same. The saving grace for this method is that each physic's mesh is derived from a common \emph{coarse mesh}, so the basis functions can be projected back to a common cell for both sets of basis functions\cite{h-Yaqi}. 

\subsection{Mesh Adaptivity}
An aspect of this Thesis is to utilize the technology of adaptive spatial discretization. It can be shown that the convergence of the L$^2$ norm is dependent on the size of mesh cells used with order $\mathcal{O}(h^{p+1})$, where $h$ is the cell size and $p$ is the oder of polynomial approximation. A more convenient metric is to compare the error to the number of unknowns in the system.

\begin{equation}
\label{eq:global}
\varepsilon \propto (N_\mathrm{dofs})^{-\frac{p+1}{d}}
\end{equation}

\noindent
Equation (\ref{eq:global}) will be used to determine whether the code is performing as expected. Refining the mesh everywhere is useful for reducing the error of a computed solution, but is not always optimal because in portions of the domain the solution produced from a finer mesh may not improve the local accuracy of the computed solution; refining the mesh in these areas will only use computational resources without much reduction in error. Say a solution is peaked in parts of the domain, but is flat in other regions; global refinement would add unknowns in the flat part of the domain that would not need it. The idea of multimesh h-AMR (``h" refers to the spatial mesh size) is to identify where refinement is needed and only refine the domain in those areas. The convergence rate for multimesh h-AMR is the same as for global refinement but the proportionality constant would be smaller --- sometimes orders of magnitude. This means the same level of error can be achieved with fewer unknowns.

\subsection{Time Dependence}
A general $s$-stage implicit Runge-Kutta method will be used to handle time dependence. Runge-Kutta methods are generally applicable to a standard problem which can be cast to include the steady state residual introduced earlier,

\begin{equation*}
\frac{\partial \vec{U}}{\partial t} = F(\vec{U},t).
\end{equation*}

\noindent
The \emph{Y}-formulation will be implemented in this Thesis which is defined as

\begin{equation*}
\vec{U}^{n+1} = \vec{U}^{n} + \tau \sum_{i=1}^s b_i F(t_n + c_i\tau, \vec{Y}_i)
\end{equation*}
\begin{equation*}
\vec{Y}_i = \vec{U}^{n} + \tau \sum_{j=1}^i a_{ij} F(t_n + c_j\tau, \vec{Y}_j), \qquad i = 1, 2, ..., s
\end{equation*}

\noindent
where $\tau$ is the step size and $a,b,c$ are given by the Butcher Tableaux characteristic of the Runge-Kutta method used. For time-dependent problems described in this Thesis, we define $F(\vec{U},t) = -F^{SS}(\vec{U},t)$. Thus the transient residual for Newton's method is given by

\begin{equation}
\label{eq:TR-res}
F_i^{TR} = \left[ \begin{array}{c} F_{\phi1}^{TR} \\ F_{\phi2}^{TR} \\ F_{C1}^{TR} \\ F_{C2}^{TR} \\ F_{T}^{TR}\end{array} \right] = 
M \vec{U}^{n+1} - M \vec{U}^n + \tau G \sum_{j=1}^i a_{ij} F^{SS}(t+c_j \tau, \vec{U}^{n+1}), \qquad i = 1, 2, ..., s,
\end{equation}

\noindent
where \emph{M} is the diagonal matrix of mass matrices for each physic, $\vec{U}$ is the solution vector and \emph{G} is the diagonal matrix of inverse time derivative coefficients. A linear combination of the transient residuals is added to the current solution to give the solution at the next time step,

\begin{equation*}
U^{n+1} = U^n + \tau \sum_{i=1}^s b_i F_i^{TR}.
\end{equation*}

\noindent
 A general Runge-Kutta method is implemented to allow higher order methods to be studied within the same framework. Several time marching methods are implemented in this Thesis which are outlined in Table \ref{RK}; each method will produce a convergence rate proportional to its order.

\begin{table}[h!]
\centering
\caption{Outline of Time Marching Methods}
\label{RK}
\begin{tabular}{|c|c|}
\hline
Backward Euler (BE) & $\mathcal{O}(\tau)$ \\ \hline
Crank-Nicholson (CN) & $\mathcal{O}(\tau^2)$ \\ \hline
SDIRK22 & $\mathcal{O}(\tau^2)$ \\ \hline
SDIRK33 & $\mathcal{O}(\tau^3)$ \\ \hline
\end{tabular}
\end{table}

\subsection{Benchmark Description}
In this Thesis it is desired to solve a problem that has applications in reactor analysis. The proposed realistic problem,\cite{ANL} from the Argonne National Lab benchmark book, involves solving Equations (\ref{eq:Gen Fast Diffusion} - \ref{eq:Gen Temp}) with a few modifications to these equations: 1) there are no external sources in the problem ($Q=0$), and 2) the thermal conductivity is zero to simulate adiabatic heating. The fast removal coefficient presents the only non-linear term in the benchmark and is described by

\begin{equation*}
	\Sigma_{r,1}(T) = \Sigma_{a,1}^0 \left[ 1 + \gamma \left( \sqrt{T} - \sqrt{T_\mathrm{ref}} \right) \right] + \Sigma_{s, 1\to2},
\end{equation*}

\noindent
where $T_\mathrm{ref}$ is the initial uniform temperature. The initial state of the system is in equilibrium, thus an initial perturbation is needed to begin the transient. The perturbation is initiated by decreasing the thermal absorption coefficient to simulate a control rod being removed. The perturbation is described by

\begin{equation*}
	\Sigma_{a,2}(t) = \Sigma_{a,2}^0  \max\{1 - 0.0606184 \* t, 0.8787631 \}.
\end{equation*}

\noindent
This simulates a ramp insertion of reactivity to the reactor model which is enough to produce a super-critical state; it takes two seconds to reach the final perturbed state. Since there is no dissipative term in the temperature equation, there is no equilibrium temperature distribution after the perturbation. The simulation is therefore terminated when the reactor power has stabilized.

\section{Research Goals}
When solving a complex problem such as non-linear coupled physics, it is expedient to first solve a simpler problem that includes many of the challenges that the larger problem contains. This allows a developer to focus on the complexities of the larger problem without the burden of solving a large system. The simpler code can be tested, by the Method of Manufactured Solutions (MMS), in an efficient way to build confidence that it will perform in a predictable way when the more complex problem is tackled. The first research goal will be

\begin{quote}
To develop and test a code to solve a simpler coupled, non-linear physics problem which includes the challenge of assembling coupling terms from physics with different spatial discretizations and non-linear coefficients.
\end{quote}

\noindent
This code will primarily be used to test that the numerical methods are solving the simple problem correctly. Convergence rates for spatial and temporal refinement will be compared to what is predicted by theory. This will also be an opportunity to check that multimesh h-AMR technology performs better than a universal spatial discretization. The hypothesis is that the time added in the assembly process for coupling terms will be recovered by solving a system with fewer unknowns.

Once the code has been verified to solve the simpler problem correctly, more physics will be added. The additional physics will be again verified by the method of manufactured solutions (MMS) and will serve as the last verification check before the larger benchmark problem is solved. The initial state of the benchmark problem is a critical reactor, thus a k-eigenvalue problem will be added to calculate the initial condition for the time-dependent problem. Solving k-eigenvalue problems have been studied for reactor models using multimesh h-AMR technology before\cite{hp-Yaqi, h-Yaqi} and similar methods will be implemented here.

The main goal of this Thesis is to determine whether, in a realistic time-dependent problem, h-AMR is a viable technique for increasing the solution process efficiency. Thus the second research goal is

\begin{quote}
To compare the solution from a fine mesh calculation to the solution from an h-AMR calculation. The comparison will include the solution error compared to memory burdens and computational time.
\end{quote}

\noindent
The hypothesis is that the fine mesh solution will produce an accurate solution but will consume much more time and memory than the h-AMR solution. The remainder of this section introduces the proposed simplified multiphysics problem and the proposed solutions to verify code correctness.

\subsection{Simplified Problem}
Tackling the full problem initially is a daunting task, but solving a smaller problem that addresses the same concerns as the larger problem is more manageable. The proposed simple problem, on the domain $[-L,L]^d$, is

\begin{equation*}
\frac{1}{v} \frac{\partial \phi}{\partial t} - \nabla D \nabla \phi + \Sigma_a \phi = S
\end{equation*}
\begin{equation*}
\rho C_p \frac{\partial T}{\partial t} - \nabla k(T) \nabla T - \kappa \phi = Q.
\end{equation*}

\noindent
This system of coupled non-linear equations addresses the major challenges with the larger benchmark problem, but is a more manageable size. The first physic is uncoupled and fully linear while the second is linearly coupled to the first and also contains a non-linear constant

\begin{equation*}
k(T) = k_0 + \frac{k_1}{k_2 + T}.
\end{equation*}

\noindent
Allowing the first physic to be linear and uncoupled will allow for the distinction between a problem globally or a problem in the handling of coupling or non-linearities. The majority of the work in this thesis will be concentrated on solving this problem correctly so that the next steps will come with relative ease. Once the framework is in place, adding more physics and solving a larger problem should only be challenged by how much computing power is available.

\subsection{Manufactured Solutions}
An effective way to determine whether a code is correctly solving the problem is to compare the solution produced to an exact solution. It is usually not a trivial task to solve these equations analytically, therefore we use the method of manufactured solutions. In this method, a solution is chosen and substituted into the equations to produce a forcing term. The system is solved with the new forcing term and the solution is compared to the exact solution. The rate of decrease of the solution error was introduced by Equation (\ref{eq:global}); if the computed convergence rates match the predicted convergence rates, success is declared. A set of exact solutions that could be used for this step are of the form

\begin{equation*}
U(\vec{x},t) = C_u \prod_{i=1}^d \left[ 1 -\left(\frac{x_i}{L}\right)^2 \right] e^{[x_i - x_i^0(t)]^2},
\end{equation*}

\noindent
where the vector $\vec{x^0}$ is different for each physic and describes the position of the Gaussian peak as a function of time. An example of the form of the position vector is

\begin{equation*}
\vec{x^0} = \left[ \begin{array}{c} r_0 cos(\omega t) \\ r_0 sin(\omega t) \\ 0  \end{array} \right].
\end{equation*}

\noindent
The manufactured solution for the two physics model is shown in Figure \ref{fig:exactSolution}. Notice the strong peaking in concentrated areas of the domain; this type of solution is where h-AMR performs well. 

\begin{figure}[h!]
\centering
\includegraphics[scale=0.42]{Graphics/Exact-Solution.png}
\caption{Manufactured solutions for the two physics problem}
\label{fig:exactSolution}
\end{figure}

\section{Preliminary Results}
\subsection{Spatial Convergence}
Some initial development has been completed on the first research goal. The convergence rates for spatial refinement and temporal refinement are considered. First the solution space is refined globally for comparison to theoretical convergence rates. For global refinement, the rate of convergence is given by Equation (\ref{eq:global}). Figure \ref{fig:global} shows the global convergence rate for the two physics problem; the solid lines shown in the graph are the theoretical convergence rates. 

\begin{figure}[h!]
\centering
\includegraphics[scale=0.50]{Graphics/Global-Convergence.png}
\caption{Global convergence for the two coupled physics problem}
\label{fig:global}
\end{figure}

\noindent
Since these convergence rates approach what is expected by Equation (\ref{eq:global}), we can test the multimesh h-AMR convergence rates. The exact solution is peaked in a small portion of the domain making this solution favorable for multimesh h-AMR and the convergence rates in Figures \ref{fig:Flux-AMR} \& \ref{fig:Temp-AMR} show this.

\begin{figure}[h!]
	\centering
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{Graphics/Flux-hAMR.png}
		\caption{Multimesh h-AMR for Physic1}
		\label{fig:Flux-AMR}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{Graphics/Temp-hAMR.png}
		\caption{Multimesh h-AMR for Physic2}
		\label{fig:Temp-AMR}
	\end{subfigure}
	\caption{Multimesh h-AMR for the two coupled physics problem}
\end{figure}

\noindent
Notice that the convergence rate is the same for each case, but the multimesh h-AMR case requires fewer unknowns for the same solution error. Also realize that the spatial meshes for each physic are not the same; if the same mesh was used for both physics, the gains from using h-AMR would not be as dramatic.

\subsection{Temporal Convergence}
The methods listed in Table \ref{RK} have been implemented for the time-dependent problem solved over a fixed time interval. The number of time steps taken is increased to produce the convergence rates shown in Figure \ref{fig:time}. As the number of time steps taken increases, the error of the final solution decreases at the predicted rate.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.50]{Graphics/Time-Convergence.png}
\caption{Time convergence for the two coupled physics problem}
\label{fig:time}
\end{figure}

\noindent
These results build confidence that the developed code is performing correctly and the research can continue as planned.

\section{Conclusions}
The initial results of this research indicate that the problem is manageable and that meaningful results can be drawn. For this research to be complete, the following list of tasks will be completed:

\begin{enumerate}
\item Develop and test a code to solve a simpler coupled, non-linear physics problem
\item Test the code again when additional physics have been incorporated
\item Compare the results from a fine mesh solution to an multimesh h-AMR solution to determine whether multimesh h-AMR is a viable technique for increasing computational efficiency on a realistic reactor physics calculation
\end{enumerate}

\noindent
The first goal is nearly complete and was shown in the \emph{Preliminary Results} section. Thus the research may continue on and address the concerns in research goal two.

\bibliographystyle{plain}
\bibliography{Proposal_Refs}

\end{document}
